package com.example.demo.payroll.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.springframework.hateoas.RepresentationModel;

import lombok.Data;

@Data
@Entity
public class Employee extends RepresentationModel<Employee> {
	
	@Id
	@GeneratedValue
	private Long id;
	private String name;
	private String role;
	
	public Employee(String name, String role) {
		super();
		this.name = name;
		this.role = role;
	}

	public Employee(){
		
	}
	
	
	
}
