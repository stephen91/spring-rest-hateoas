package com.example.demo.payroll.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.payroll.EmployeeRepository;
import com.example.demo.payroll.entity.Employee;
import com.example.demo.payroll.exception.EmployeeNotFoundException;

@RestController
@RequestMapping("/employees")
public class EmployeeController {
	
	private final Link allEmployeesLink = linkTo(EmployeeController.class).withRel("/employees");
	
	@Autowired
	EmployeeRepository repository;
	
	@GetMapping()
	public CollectionModel<Employee> getEmployees(){
		return new CollectionModel<>(repository.findAll(), allEmployeesLink.withSelfRel());
	}
	
	@GetMapping("/{id}")
	public EntityModel<Employee> getOne(@PathVariable Long id) throws EmployeeNotFoundException {
		Employee employee = repository.findById(id).orElseThrow(() -> new EmployeeNotFoundException(id));
		return new EntityModel<>(employee, linkTo(EmployeeController.class).slash(id).withSelfRel()
				,allEmployeesLink);
	}
	
	@PostMapping()
	public EntityModel<Employee> addNewEmployee(@RequestBody Employee employee) {
		return new EntityModel<>(repository.save(employee)
				, linkTo(EmployeeController.class).slash(employee.getId()).withSelfRel(), allEmployeesLink);
	}
	
	@PutMapping("/{id}")
	public EntityModel<Employee> updateEmployee(@RequestBody Employee newEmployee, @PathVariable Long id) throws EmployeeNotFoundException {
		Optional<Employee> existingEmployee = repository.findById(id);
		if(existingEmployee.isEmpty()) {
			throw new EmployeeNotFoundException(id);
		} else {
			newEmployee.setId(id);
			return new EntityModel<>(repository.save(newEmployee), linkTo(EmployeeController.class).slash(id).withSelfRel()
					,allEmployeesLink);
		}
			
	}
	
	@DeleteMapping("/{id}")
	public EntityModel<Employee> deleteEmp(@PathVariable Long id) {
		repository.deleteById(id);
		return new EntityModel<>(new Employee(), allEmployeesLink);
	}
}

