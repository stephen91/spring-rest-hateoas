package com.example.demo.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestfulWebservicesGuideApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestfulWebservicesGuideApplication.class, args);
	}

}
