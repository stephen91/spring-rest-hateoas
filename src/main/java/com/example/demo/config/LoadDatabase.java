package com.example.demo.config;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.example.demo.payroll.EmployeeRepository;
import com.example.demo.payroll.entity.Employee;

import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
public class LoadDatabase {
	
	@Bean
	CommandLineRunner initDatabase(final EmployeeRepository employeeRepo) {
		return args -> {
			log.info("Preloading "+ employeeRepo.save(new Employee("Bilbo Baggins", "Thief")));
			log.info("Preloading "+ employeeRepo.save(new Employee("Frodo Baggins", "Burglar")));
		};
	}
}
